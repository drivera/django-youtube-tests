from django.test import TestCase, SimpleTestCase
from .ytparser import YTParser
from . import views
from .testdata import datavideos

import urllib.request
# Create your tests here.

class TestParser(TestCase):

        def setUp(self):
            url = "ytinfo/testdata/testchannel.xml"
            self.xmlStream = open(url, 'r')
            self.channel = {
                            'cname': 'CursosWeb',
                            'clink': 'https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg'
                            }
            self.videos = datavideos.videos

        def test_channelparsing(self):
            yt = YTParser(self.xmlStream)
            response=yt.channel()
            self.assertEquals(response,self.channel)

        def test_videoparsing(self):
            yt = YTParser(self.xmlStream)
            response=yt.videos()
            self.assertEquals(response,self.videos)

class TestViews(TestCase):
    def setUp(self):
        self.videos = datavideos.videosSelect
        self.videosReduced = [
            {"id":"SPLwYv62-og","title":"JSON práctico","selected": False},
            {"id":"MyaRcqzBbk4","title":"Django práctico: testing","selected":True},
            {"id":"5dU7O35tFrc","title":"Frikiminutos: Raspberry Pi","selected":False,},
        ]


    def test_videoInfoReduction(self):
        response=views.videoInfoReduction(self.videos)
        self.assertEquals(response,self.videosReduced)

    def test_getmain(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.main)
        self.assertInHTML("<h1> Videos Seleccionados </h1>", response.content.decode(encoding='UTF-8'))
        self.assertInHTML("<h1> Videos Seleccionables </h1>", response.content.decode(encoding='UTF-8'))
        self.assertInHTML("<input type='submit' value='Select' name='action'>", response.content.decode(encoding='UTF-8'))

    def test_postmain(self):
        response = self.client.post('/',{'id': "No existe este id"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.main)


    def test_getytvideo(self):
        response = self.client.get('/SPLwYv62-og')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.ytvideo)
        self.assertInHTML('<a href="/"> Volver atrás </a>', response.content.decode(encoding='UTF-8'))
