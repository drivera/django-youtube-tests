videos = [
    {
        "id":"SPLwYv62-og",
        "title":"JSON práctico",
        "link":"https://www.youtube.com/watch?v=SPLwYv62-og",
        "pdate":"2020-04-18T12:34:11+00:00",
        "img":"https://i4.ytimg.com/vi/SPLwYv62-og/hqdefault.jpg",
        "description":"Leer documentos JSON desde Python, para poder manejarlos como variables dentro de nuestro programa (listas, diccionarios...), es muy fácil. Te lo explicamos en este ejemplo.",
    },
    {
        "id":"MyaRcqzBbk4",
        "title":"Django práctico: testing",
        "link":"https://www.youtube.com/watch?v=MyaRcqzBbk4",
        "pdate":"2020-04-17T17:38:36+00:00",
        "img":"https://i2.ytimg.com/vi/MyaRcqzBbk4/hqdefault.jpg",
        "description":'Vamos a ver cómo se pueden hacer tests unitarios "a la manera Django". Muy útil para comprobar nuestro código, y nuestra API HTTP, según vamos desarrollando nuestra aplicación Django.',
    },
    {
        "id":"5dU7O35tFrc",
        "title":"Frikiminutos: Raspberry Pi",
        "link":"https://www.youtube.com/watch?v=5dU7O35tFrc",
        "pdate":"2020-04-02T17:05:53+00:00",
        "img":"https://i2.ytimg.com/vi/5dU7O35tFrc/hqdefault.jpg",
        "description":"La Raspberry Pi es un ordenador pequeñito, barato, y muy potente. Muy interesante para realizar proyectos de aficionado, pero también para trabajar en serio.",
    }
]

videosSelect = [
    {
        "id":"SPLwYv62-og",
        "title":"JSON práctico",
        "link":"https://www.youtube.com/watch?v=SPLwYv62-og",
        "pdate":"2020-04-18T12:34:11+00:00",
        "img":"https://i4.ytimg.com/vi/SPLwYv62-og/hqdefault.jpg",
        "description":"Leer documentos JSON desde Python, para poder manejarlos como variables dentro de nuestro programa (listas, diccionarios...), es muy fácil. Te lo explicamos en este ejemplo.",
        "selected": False
    },
    {
        "id":"MyaRcqzBbk4",
        "title":"Django práctico: testing",
        "link":"https://www.youtube.com/watch?v=MyaRcqzBbk4",
        "pdate":"2020-04-17T17:38:36+00:00",
        "img":"https://i2.ytimg.com/vi/MyaRcqzBbk4/hqdefault.jpg",
        "description":'Vamos a ver cómo se pueden hacer tests unitarios "a la manera Django". Muy útil para comprobar nuestro código, y nuestra API HTTP, según vamos desarrollando nuestra aplicación Django.',
        "selected": True
    },
    {
        "id":"5dU7O35tFrc",
        "title":"Frikiminutos: Raspberry Pi",
        "link":"https://www.youtube.com/watch?v=5dU7O35tFrc",
        "pdate":"2020-04-02T17:05:53+00:00",
        "img":"https://i2.ytimg.com/vi/5dU7O35tFrc/hqdefault.jpg",
        "description":"La Raspberry Pi es un ordenador pequeñito, barato, y muy potente. Muy interesante para realizar proyectos de aficionado, pero también para trabajar en serio.",
        "selected": False
    }
]
